# Rapchat Web App

## Build Setup


``` bash
# install dependencies
$ npm install # Or yarn install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, checkout the [Nuxt.js docs](https://github.com/nuxt/nuxt.js).

## A/B Testing Primer

PlanOut [Framework](https://facebook.github.io/planout/)<br>
PlanOut [Implementation](https://github.com/HubSpot/PlanOut.js)<br>
Amplitude (https://www.amplitude.com)

1. Start by reading the docs for both PlanOut and Amplitude. Once you have a good idea of how they both work continue.
2. Initalize your experiment according to the planout docs and choose your variations. Put your experiment set up in ~/utils/ab-tester.js
3. Note, we can't access the Amplitude snippet unless we are in the browser process so check before trying to get the deviceId or sending any data to Amplitude.
```javascript
if (process.browser){
  // test goes in here
}
```
4. Initalize Amplitude, set the deviceId, and pass it in to get the variation for the given user_id. You don't need to keep track of this, as the hashing algorithm is deterministic (a userId input always outputs the same result).
```javascript
amplitude.init('api key')
var userId = amplitude.setDeviceId()
amplitude.init('api key', null null, function(instance){
  // set up test and get the variation
  var test = new PlanoutTest({userId:userId})
  if (test) {
    // show the variation
  }
})
```
5. It's a good idea to hide the content you are testing until you have the variation set and then revealing it, in order to prevent flash of the default variation. I put this in the mounted() lifecycle hook but if you can find a better solution feel free to try it out.
```javascript
mounted() {
    // this.contentHidden = true
    runTest()
    // this.contentHidden = false
}
```
6. Make sure you are tracking both the default, and the test case, so we can see if we actually improved. Also be sure you are tracking every PageView so you can calculate conversion data. Use an [A/B testing](https://abtestguide.com/calc/) calculator to verify the validity your results.
7. Have fun! If you find a way to improve any of these processes please do so and document your changes!

import axios from 'axios'

if (process.env.NODE_ENV != 'production') {
  var instance = axios.create({
      baseURL: 'https://apidev.rapchat.me/api'
  });
} else {
  var instance = axios.create({
      baseURL: 'https://api.rapchat.me/api'
  });
}

export default instance

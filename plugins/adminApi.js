import axios from 'axios'

if (process.env.NODE_ENV != 'production') {
  var instance = axios.create({
      baseURL: 'https://adminapidev.rapchat.me/api',
  });
} else {
  var instance = axios.create({
      baseURL: 'https://adminapi.rapchat.me/api'
  });
}

export default instance

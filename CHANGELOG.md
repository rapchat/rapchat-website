# Changelog

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/), the versioning system is [CalVer](https://calver.org/) in format YYYY-MM-0D.

## Unreleased

##2018-09-26
### added
- search raps by hashtags

### changed
- beat and rap detail redesign!
- trim titles on rap cards


## 2018-08-21
### Changed
- users now see 'uploading...' on the submit button when a beat is upload
- modal now clears out immediately after upload success

## 2018-08-23
### Added
- Producers platform: users can now create accounts, and  upload beats directly to the app.
- submit beat call to action header on beats page
- added CHANGELOG.md 😁
- minimize the player so it's not in the way!
- trending raps on header nav bar

### Changed
- beat titles and artists now link to their respective detail cards
- the nav bar now shows logged in producers
- submit beats now goes to the internal submission platform instead of the cognito form

## 2018-07-31
### Changed
- promoted popover test to default behavior. users will now see a popover after clicking play on a rap and a delay of 8 seconds.
- new styles on rapname generator

import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const _4ab91f36 = () => import('../pages/beats/index.vue' /* webpackChunkName: "pages/beats/index" */).then(m => m.default || m)
const _513628d9 = () => import('../pages/contests.vue' /* webpackChunkName: "pages/contests" */).then(m => m.default || m)
const _58320702 = () => import('../pages/download.vue' /* webpackChunkName: "pages/download" */).then(m => m.default || m)
const _20287e9b = () => import('../pages/feed/index.vue' /* webpackChunkName: "pages/feed/index" */).then(m => m.default || m)
const _7aa6c960 = () => import('../pages/forgot/index.vue' /* webpackChunkName: "pages/forgot/index" */).then(m => m.default || m)
const _3edca4b4 = () => import('../pages/licensing.vue' /* webpackChunkName: "pages/licensing" */).then(m => m.default || m)
const _0c2497bc = () => import('../pages/login/index.vue' /* webpackChunkName: "pages/login/index" */).then(m => m.default || m)
const _d7df8404 = () => import('../pages/privacy.vue' /* webpackChunkName: "pages/privacy" */).then(m => m.default || m)
const _2a4502da = () => import('../pages/producers/index.vue' /* webpackChunkName: "pages/producers/index" */).then(m => m.default || m)
const _62a76721 = () => import('../pages/rapnamegenerator.vue' /* webpackChunkName: "pages/rapnamegenerator" */).then(m => m.default || m)
const _b8ace916 = () => import('../pages/rapup/index.vue' /* webpackChunkName: "pages/rapup/index" */).then(m => m.default || m)
const _2f0a973a = () => import('../pages/rcradio/index.vue' /* webpackChunkName: "pages/rcradio/index" */).then(m => m.default || m)
const _0bd5abfd = () => import('../pages/terms.vue' /* webpackChunkName: "pages/terms" */).then(m => m.default || m)
const _028a05c2 = () => import('../pages/trending.vue' /* webpackChunkName: "pages/trending" */).then(m => m.default || m)
const _6d3f2afb = () => import('../pages/landing/beats.vue' /* webpackChunkName: "pages/landing/beats" */).then(m => m.default || m)
const _f5bf54c2 = () => import('../pages/landing/producers.vue' /* webpackChunkName: "pages/landing/producers" */).then(m => m.default || m)
const _225bd77a = () => import('../pages/landing/Studio2.vue' /* webpackChunkName: "pages/landing/Studio2" */).then(m => m.default || m)
const _2951771e = () => import('../pages/beats/_id.vue' /* webpackChunkName: "pages/beats/_id" */).then(m => m.default || m)
const _443c344b = () => import('../pages/forgot/_id/index.vue' /* webpackChunkName: "pages/forgot/_id/index" */).then(m => m.default || m)
const _1ecd20c5 = () => import('../pages/producers/_id/index.vue' /* webpackChunkName: "pages/producers/_id/index" */).then(m => m.default || m)
const _6681dc6c = () => import('../pages/profile/_id.vue' /* webpackChunkName: "pages/profile/_id" */).then(m => m.default || m)
const _555282d2 = () => import('../pages/raps/_id.vue' /* webpackChunkName: "pages/raps/_id" */).then(m => m.default || m)
const _38c431c6 = () => import('../pages/rapup/_id.vue' /* webpackChunkName: "pages/rapup/_id" */).then(m => m.default || m)
const _12c8e180 = () => import('../pages/share/_id.vue' /* webpackChunkName: "pages/share/_id" */).then(m => m.default || m)
const _61d7edc8 = () => import('../pages/index.vue' /* webpackChunkName: "pages/index" */).then(m => m.default || m)
const _6a22db83 = () => import('../pages/_error.vue' /* webpackChunkName: "pages/_error" */).then(m => m.default || m)



if (process.client) {
  window.history.scrollRestoration = 'manual'
}
const scrollBehavior = function (to, from, savedPosition) {
  // if the returned position is falsy or an empty object,
  // will retain current scroll position.
  let position = false

  // if no children detected
  if (to.matched.length < 2) {
    // scroll to the top of the page
    position = { x: 0, y: 0 }
  } else if (to.matched.some((r) => r.components.default.options.scrollToTop)) {
    // if one of the children has scrollToTop option set to true
    position = { x: 0, y: 0 }
  }

  // savedPosition is only available for popstate navigations (back button)
  if (savedPosition) {
    position = savedPosition
  }

  return new Promise(resolve => {
    // wait for the out transition to complete (if necessary)
    window.$nuxt.$once('triggerScroll', () => {
      // coords will be used if no selector is provided,
      // or if the selector didn't match any element.
      if (to.hash) {
        let hash = to.hash
        // CSS.escape() is not supported with IE and Edge.
        if (typeof window.CSS !== 'undefined' && typeof window.CSS.escape !== 'undefined') {
          hash = '#' + window.CSS.escape(hash.substr(1))
        }
        try {
          if (document.querySelector(hash)) {
            // scroll to anchor by returning the selector
            position = { selector: hash }
          }
        } catch (e) {
          console.warn('Failed to save scroll position. Please add CSS.escape() polyfill (https://github.com/mathiasbynens/CSS.escape).')
        }
      }
      resolve(position)
    })
  })
}


export function createRouter () {
  return new Router({
    mode: 'history',
    base: '/',
    linkActiveClass: 'nuxt-link-active',
    linkExactActiveClass: 'nuxt-link-exact-active',
    scrollBehavior,
    routes: [
		{
			path: "/beats",
			component: _4ab91f36,
			name: "beats"
		},
		{
			path: "/contests",
			component: _513628d9,
			name: "contests"
		},
		{
			path: "/download",
			component: _58320702,
			name: "download"
		},
		{
			path: "/feed",
			component: _20287e9b,
			name: "feed"
		},
		{
			path: "/forgot",
			component: _7aa6c960,
			name: "forgot"
		},
		{
			path: "/licensing",
			component: _3edca4b4,
			name: "licensing"
		},
		{
			path: "/login",
			component: _0c2497bc,
			name: "login"
		},
		{
			path: "/privacy",
			component: _d7df8404,
			name: "privacy"
		},
		{
			path: "/producers",
			component: _2a4502da,
			name: "producers"
		},
		{
			path: "/rapnamegenerator",
			component: _62a76721,
			name: "rapnamegenerator"
		},
		{
			path: "/rapup",
			component: _b8ace916,
			name: "rapup"
		},
		{
			path: "/rcradio",
			component: _2f0a973a,
			name: "rcradio"
		},
		{
			path: "/terms",
			component: _0bd5abfd,
			name: "terms"
		},
		{
			path: "/trending",
			component: _028a05c2,
			name: "trending"
		},
		{
			path: "/landing/beats",
			component: _6d3f2afb,
			name: "landing-beats"
		},
		{
			path: "/landing/producers",
			component: _f5bf54c2,
			name: "landing-producers"
		},
		{
			path: "/landing/Studio2",
			component: _225bd77a,
			name: "landing-Studio2"
		},
		{
			path: "/beats/:id",
			component: _2951771e,
			name: "beats-id"
		},
		{
			path: "/forgot/:id",
			component: _443c344b,
			name: "forgot-id"
		},
		{
			path: "/producers/:id",
			component: _1ecd20c5,
			name: "producers-id"
		},
		{
			path: "/profile/:id?",
			component: _6681dc6c,
			name: "profile-id"
		},
		{
			path: "/raps/:id?",
			component: _555282d2,
			name: "raps-id"
		},
		{
			path: "/rapup/:id",
			component: _38c431c6,
			name: "rapup-id"
		},
		{
			path: "/share/:id?",
			component: _12c8e180,
			name: "share-id"
		},
		{
			path: "/",
			component: _61d7edc8,
			name: "index"
		},
		{
			path: "/:error",
			component: _6a22db83,
			name: "error"
		}
    ],
    
    
    fallback: false
  })
}

const nodeExternals = require('webpack-node-externals')

module.exports = {
    env: {
        domain: 'https://www.rapchat.me',
    },
    head: {
        title: 'Rapchat',
        meta: [
            {charset: 'utf-8'},
            {name: 'viewport', content: 'width=device-width, initial-scale=1'},
            {hid: 'og:site_name', name: 'og:site_name',content: 'Rapchat'},
            {hid: 'og:title', name: 'og:title', content: 'Rapchat' },
            {hid: 'og:description', name: 'og:description', content: 'Record raps over beats and share them anywhere on the Rapchat app. Pick from thousands of free instrumentals with the top mobile recording studio in the game!'},
            {hid: 'description', name: 'description', content: 'Record and share raps on the Rapchat app. Pick from thousands of free beats and instrumentals. Check out the top mobile recording studio in the game!'}
        ],
        script: [
            { id:'ze-snippet', src: 'https://static.zdassets.com/ekr/snippet.js?key=90bded08-db00-45d9-b8f1-62d79d9c379b' }
        ],
        link: [
            {rel: 'icon', type: 'image/x-icon', href: '/favicon.png'},
            {rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Roboto:100,300,400'}
        ]
    },
    /*
    ** Customize the progress bar color
    */
    loading: {color: '#8C7F4A'},
    modules: [
        'bootstrap-vue/nuxt'
    ],
    plugins: [
        {
            src:'~/plugins/fbPixel.js',
            ssr: false
        },
        {
            src: '~/plugins/amplitude.js',
            ssr: false
        },
        {
            src: '~/plugins/gtm.js',
            ssr: false
        },
        {
            src: '~/plugins/branch.js',
            ssr: false
        },
        {
            src: '~/plugins/moment.js'
        },
        {
            src: '~plugins/modal',
            ssr: false
        },
        {
          src: '~plugins/vue-awesome.js'
        }
    ],
    build: {
        extractCSS: true,
        vendor: ['vue-awesome'],
        extend (config, {isServer}) {
          if (isServer) {
            config.externals = [
              nodeExternals({
                 whitelist: [/^vue-awesome/]
              })
            ]
          }
        }
    },
    render: {
        resourceHints: false,
    }
}

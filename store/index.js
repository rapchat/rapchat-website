import Vuex from 'vuex'
import axios from '~/plugins/axios'
import { ShareRapPageTest, ButtonCopyTest } from '~/utils/ab-tester.js'

const cookie = require('cookie')

const checkIfMobile = (context) => {
  const userAgent = process.server ? context.req.headers['user-agent'] : navigator.userAgent
  var MobileDetect = require('mobile-detect')
  var md = new MobileDetect(userAgent)
  var deviceType = md.os()
  return {isMobile: !!md.mobile(), deviceType: deviceType}
}
const findArrayItemByKey = (array, item, key) => {
  if (array.length > 0) {
    const searchItem = (i) => {
      return i[key] === item
    }
  }
  let index = array.findIndex(searchItem)
  return index
}
const createStore = () => {
    return new Vuex.Store({
        state: {
            song: '',
            video: '',
            player: true,
            isPlaying: false,
            songs: [],
            feed: {
              following: [],
              featured: [],
              beats: []
            },
            suggestedUsers: [],
            currentUser: null,
            authenticated: false,
            userToken: null,
            isMobile: null,
            deviceType: null,
            playerView: null,
            uploadBeats: [],
            uploadStatus: null,
            shareVariation: false,
            shareExp: null,
            copyVariation: 'JOIN THE GAME',
            copyExp: null
        },
        getters: {
            song: state => state.song,
            isPlaying: state => state.isPlaying,
            songs: state => state.songs,
            userToken: state => state.userToken,
            isMobile: state => state.isMobile,
            playerView: state => state.playerView, //update player view based on route
            uploadBeats: state => state.uploadBeats,
            uploadStatus: state => state.playerView,
            feed: state => state.feed,
            suggestedUsers: state => state.suggestedUsers
        },
        setters: {
          isPlaying: state => state.isPlaying
        },
        actions: {
            async nuxtServerInit ({commit, dispatch, state}, context) {
                // isMobile
                commit('set', checkIfMobile(context))

                //init a/b tests here to avoid server/client mismatched renders
                if (context.req.headers.cookie) {
                    let cookies = cookie.parse(context.req.headers.cookie)
                    let gaToken = cookies['_ga']
                    if (gaToken) {
                      // You can initialize a/b tests here. using GA token should give a pretty high hit rate.
                    }
                }

                // keeps the user logged in between sessions
                if (!state.producer) {
                  let cookies = ''
                  let token = ''
                  if (context.req.headers.cookie) {
                    cookies = cookie.parse(context.req.headers.cookie)
                    token = cookies['rc-token']
                  }
                  if (token) {
                    await axios.get('/producers/verifyToken',
                      {headers:
                        {'X-ZUMO-AUTH':token}
                      }).then((res) => {
                        if (res.status === 200) {
                          res.data.data.token = token
                          res.data.data.isProducer = true
                          return dispatch('loginUser', res.data.data)
                        }
                      }).catch(e => {
                        console.log(e)
                      })
                  }
                }
            },
            loginUser ({commit}, user) {
              // @TODO do the api calls here
              commit('setCurrentUser', user)
            },
            logoutUser ({commit}, user) {
              commit('removeCurrentUser')
            },
            authenticateUser({commit}, auth) {
              commit('authenticate', auth)
            },
            setSuggestedUsers ({commit}, users) {
              //load the suggested users here
              commit('setSuggested', users)
            },
            playSong () {
              //
            },
            nextSong({commit, state}) {
              if (state.songs.length > 0) {
                const searchId = (item) => {
                  return item.id === state.song.id
                }
                let index = state.songs.findIndex(searchId)
                if (index >= 0) {
                  if (state.songs.length >= (index+2)) {
                    commit('updateSong', state.songs[index+1])
                    commit('updateView', state.songs[index+1].type)
                  }
                } else { // track is not in the list.
                  commit('updateSong', state.songs[0])
                  commit('updateView', state.songs[0].type)
                }
              }
            },
            previousSong ({commit, state}) {
              if (state.songs.length > 0) {
                const searchId = (item) => {
                  return item.id === state.song.id
                }
                let index = state.songs.findIndex(searchId)
                if (index === 0) {
                  commit('updateSong', state.songs[0])
                  commit('updateView', state.songs[0].type)
                } else if (index > 0) {
                  commit('updateSong', state.songs[index-1])
                  commit('updateView', state.songs[index-1].type)
                }
              }
            },
            likeRap ({state}, args) {
              return axios({
                method: "POST",
                url: '/rapvote',
                headers: {'x-zumo-auth': state.currentUser.token},
                data: {
                  rapId: args.rapId,
                  like: args.liked
                }
              })
            },
            likeBeat ({state}, args) {
              return axios({
                method: "post",
                url: '/beats/favorite',
                headers: {'x-zumo-auth': state.currentUser.token},
                data: {
                  beatId: args.beatId,
                  favorite: args.favorite
                }
              })
            },
            getBeatComments ({state}, args) {
              return axios({
                headers: {'x-zumo-auth': state.currentUser.token},
                method: 'GET',
                url: `/beats/comment/${args.beatId}`
              })
            },
            commentBeat ({state}, args) {
              return axios({
                method: 'POST',
                url: '/beats/comment',
                headers: {'x-zumo-auth': state.currentUser.token},
                data: {
                  beatId: args.beatId,
                  comment: args.comment
                }
              }).catch(e => {
                console.log(e)
              })
            },
            getRapComments ({state}, args) {
              return axios({
                headers: {'x-zumo-auth': state.currentUser.token},
                method: 'GET',
                url: '/comment',
                params: {rapid: args.rapid}
              })
            },
            commentRap ({state}, args) {
              return axios({
                method: 'POST',
                url: "/comment",
                headers: {'x-zumo-auth': state.currentUser.token},
                data: {
                  rapid: args.rapId,
                  comment: args.comment
                }
              }).catch(e => {
                console.log(e)
              })
            },
            followUser ({state}, args) {
              return axios({
                method: 'post',
                url: '/follow',
                headers: {'x-zumo-auth': state.currentUser.token},
                data: {id: args.userId}
              }).catch(e => {
                console.log(e)
              })
            }
        },
        mutations: {
            updateSong (state, song) {
              state.player = true
              state.song = song
            },
            updateVideo (state, video) {
              state.player = false
              state.video = video
            },
            updateView (state, playerView) {
              state.playerView = playerView
            },
            addSong (state, song) {
              state.songs.push(song)
            },
            setSongs (state, songs) {
              state.songs = songs
            },
            togglePlaying(state) {
              state.isPlaying = !state.isPlaying
            },
            addBeatUpload (state, value) {
              state.uploadBeats.push(value)
            },
            setCurrentUser (state, user) {
              state.currentUser = user
              state.authenticated = true
              state.token = user.token
              if (process.browser) {
                let savedToken = document.cookie.split(';').filter((item) => item.includes('rc-token=')).length
                if (!savedToken) {
                  let token = `rc-token=${state.token}; max-age=${60*60*24*365}; path=/;`
                  document.cookie = token
                }
              }
            },
            removeCurrentUser (state) {
              let savedToken = document.cookie.split(';').filter((item) => item.includes('rc-token='))
              document.cookie = `rc-token=${state.token};expires=Thu, 01 Jan 1970 00:00:01 GMT;path=/;`;
              state.currentUser = null
              state.authenticated = false
              state.token = null

            },
            authenticate(state, auth) {
              state.authenticated = auth
            },
            setFeed (state, args) {
              state.feed[args.type] = args.items
            },
            setSuggested (state, users) {
              state.suggestedUsers = users
            },
            set (state, value) {
              state.isMobile = value.isMobile
              state.deviceType = value.deviceType
            }
        }
    })
}
export default createStore

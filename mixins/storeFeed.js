export default {
    created() {
        this.parseFeed()
    },
    watch: {
        items() {
            this.parseFeed()
        }
    },
    methods: {
        parseFeed() {
            if (this.items && this.items.length>0) {
              this.$store.commit('setFeed', {items: this.items, type: this.active})
            }
        }
    }
}

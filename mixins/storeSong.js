export default {
    created() {
        this.parseSongs()
    },
    watch: {
        songs() {
            this.parseSongs()
        }
    },
    methods: {
        parseSongs() {
            let songs = []
            if (this.songs && this.songs.length>0) {
              this.songs.map(song => {
                  let s
                  if (song.type === 'beat') {
                      s = {
                          _id: song._id,
                          id: song._id,
                          beatId: song.options[0]._id,
                          title: song.title,
                          image: song.imagefile,
                          artist: song.artist,
                          audio: song.blobname,
                          producerId: song.producerId,
                          blobname: song.blobname,
                          status: song.status,
                          raps_count: song.raps_count,
                          playsCount: song.playsCount,
                          type: 'beat'
                      }
                  } else {
                      s = {
                          _id: song._id,
                          id: song.id,
                          title: song.name,
                          image: song.image,
                          artist: song.owner.username,
                          profilephoto: song.owner.profilephoto,
                          audio: song.id,
                          likes: song.likes,
                          plays: song.plays,
                          blobname: song.id,
                          filters: song.filters || [],
                          type: 'rap'
                      }
                  }
                  if(song.prices) s.prices = song.prices
                  s.userId = (song.userId && song.userId != '') ? song.userId : ''
                  songs.push(s)
                  this.$store.commit('setSongs', songs)
              })
            }
            this.songss = songs
        }
    }
}

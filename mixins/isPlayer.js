import axios from '~/plugins/axios'

export default{
    props: {
      item: {type: Object, required: true},
      type: {type: String, required: false}
    },
  }
//     data() {
//         return {
//             trackQueue: [],
//             songIndex: 0,
//             currentSong: null,
//             playerInstance: 0,
//             first: 0,
//             duration: 0,
//             currentTime: 0,
//             playerCreated: false,
//             repeat: false,
//             seekedTo: 0,
//             playCounted:false
//         };
//     },
//     watch:{
//       '$route': 'closePlayer',
//       currentTime() {
//         if (this.playerInstance.currentTime >= 10 && this.playing && !this.playCounted && !this.seekedTo){
//           this.incrementPlayCount(this.item, this.type)
//         }else if(this.seekedTo && !this.playCounted){
//           if(this.playerInstance.currentTime - this.seekedTo >= 10){
//             this.incrementPlayCount(this.item, this.type)
//           }
//         }
//       },
//     },
//     // mounted() {
//     //     this.createAudioPlayer()
//     //     this.playerInstance.ontimeupdate = this.updateSeek
//     // },
//     // destroyed () {
//     //   this.currentSong = null
//     //   this.playing = false
//     //   this.playerInstance.pause()
//     // },
//     methods: {
//         // createAudioPlayer(){
//         //   this.playerInstance = document.createElement("AUDIO");
//         //   var track = this.item
//         //   if (track.artist) { //beats - artist is a property on beats and not raps
//         //     track.id = track._id
//         //     track.audio = `https://cdn.rapchat.me/beats/${track.blobname}.m4a`
//         //   } else { //raps
//         //     track.audio = `https://cdn.rapchat.me/raps/${track.id}`
//         //   }
//         //   this.playerInstance.setAttribute('src', track.audio)
//         //   this.playerInstance.setAttribute('preload', 'metadata')
//         //   this.setAudioDuration()
//         //   this.playerCreated = true
//         // },
//         // setAudioDuration(){
//         //   let _this = this
//         //   this.playerInstance.addEventListener('loadedmetadata', function() {
//         //     var seconds = this.duration
//         //     var duration = _this.formatTime(seconds)
//         //     _this.duration = duration
//         //     _this.currentTime = _this.formatTime(0)
//         //   })
//         // },
//         // createSeekBar(){
//         //   let seekBar = document.getElementById('seek-bar-player')
//         //   let _this = this;
//         //   seekBar.addEventListener('input', function () {
//         //       if (_this.playerInstance) {
//         //           if (_this.playerInstance.paused) {
//         //               _this.playing = true
//         //               _this.playerInstance.play();
//         //           }
//         //           let seconds = _this.playerInstance.duration * seekBar.value / 100
//         //           _this.playerInstance.currentTime = seconds
//         //           _this.changeSeek(seconds)
//         //       }
//         //   })
//         // },
//         formatTime(seconds) {
//             seconds = Math.ceil(seconds)
//             let minutes = Math.floor(seconds / 60)
//             let sec = Math.round(seconds - minutes * 60)
//             if (sec < 10) sec = '0' + sec
//             return minutes + ':' + sec
//         },
//         togglePlay(e) {
//             e.preventDefault();
//             let _this = this;
//             if (!this.playerCreated) {
//                 this.playSound(this.item)
//             } else {
//                 this.playing = !this.playing
//                 this.playerInstance.paused ? this.playerInstance.play() : this.playerInstance.pause();
//             }
//             this.playerInstance.onended = () => {
//                 if(_this.repeat) {
//                     _this.changeSeek(0)
//                     _this.playing = true
//                 } else {
//                     _this.playing = false
//                 }
//             };
//         },
//         updateSeek() {
//             this.seekTrack(this.playerInstance.currentTime)
//             this.currentTime = this.formatTime(this.playerInstance.currentTime)
//             let val = this.playerInstance.currentTime / this.playerInstance.duration
//             let seekBar = document.getElementById('seek-bar-player')
//             seekBar.style.backgroundImage = '-webkit-gradient(linear, left top, right top, '
//                 + 'color-stop(' + val + ', #8c8046), '
//                 + 'color-stop(' + val + ', #777)'
//                 + ')'
//         },
//         seekTrack(seconds) {
//             let percentageComplete = seconds / this.playerInstance.duration * 100
//             let seekBar = document.getElementById('seek-bar-player')
//             seekBar.value = percentageComplete
//         },
//         changeSeek(seconds) {
//             this.playerInstance.paused && this.playerInstance.play();
//             this.seekTrack(seconds)
//             this.seekedTo = this.playerInstance.currentTime
//         },
//         playSound(track) {
//             this.playing = true
//             this.playerInstance.play();
//         },
//         incrementPlayCount(track, type) {
//           if (track.type === 'beat' || type ==='beat'){ //artist is only an attr of beats
//             track.id = track._id
//             axios.post('/beats/plays', {beatId: track.options[0]._id}).then(function (response) {
//               console.log(response);
//             }).catch(function (error) {
//               console.log(error);
//             });
//           } else { //raps
//             axios.post('/rapincreaseplays', {rapid: track.id}).then(function (response) {
//                 console.log(response);
//             })
//             .catch(function (error) {
//             });
//           }
//           this.playCounted = true
//         },
//         closePlayer () {
//           console.log('stopping music')
//           this.currentSong = null
//           this.playing = false
//           this.playerInstance.pause()
//         }
//     }
// }

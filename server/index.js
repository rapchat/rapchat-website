import express from 'express'
import { Nuxt, Builder } from 'nuxt'

const session = require('express-session')
const app = express()
const host = process.env.HOST || '127.0.0.1'
const port = process.env.port || '8000'
var http = require('http')

app.use(session({
  secret: 'eba1afbf-6655-48f2-b131-8f174810a903',
  resave: false,
  saveUninitialized: false,
  cookie: { maxAge: 60000 }
}))

app.use(express.static('static'));

app.set('port', port)

let config = require('../nuxt.config.js')
config.dev = !(process.env.NODE_ENV === 'production')

const nuxt = new Nuxt(config)

// Build only in dev mode
if (config.dev) {
  const builder = new Builder(nuxt)
  builder.build()
}

function normalizePort(val) {
var port = parseInt(val, 10);

console.log('PORT: ' + port) // eslint-disable-line no-console

 if (isNaN(port)) { // named pipe
      return val; }

 if (port >= 0) { // port number
        return port; }
   return false; }

function onError(error) { if (error.syscall !== 'listen') {
 throw error; } var bind = typeof port === 'string' ? 'Pipe ' + port : 'Port ' + port; // handle specific listen errors with friendly messages
 switch (error.code) {
   case 'EACCES': console.error(bind + ' requires elevated privileges');
   process.exit(1); break;
   case 'EADDRINUSE': console.error(bind + ' is already in use');
   process.exit(1); break; default: throw error; } } /** * Event listener for HTTP server "listening" event. */

   function onListening() {
     var addr = server.address();
     var bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port;
     console.log('Listening on ' + bind);
}

app.use(nuxt.render)
var server = http.createServer(app);

server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

console.log('Server listening on ' + host + ':' + port) // eslint-disable-line no-console
console.log('PORT: ' + process.env.PORT) // eslint-disable-line no-console

import howler from 'howler'
export default{
    playerInstance: null,
    currentSecond: 0,
    play (track, _this) {
        let player = this
        this.playerInstance = new Howl({
            src: [track.audio],
            html5: true,
            autoplay: true,
            volume: 1,
            onload() {
                _this.playing = true;
                _this.currentSong = track
                _this.currentSong.duration = this.duration();
                requestAnimationFrame(player.updateSeek(player));
            },
            onend() {
                _this.playNextTrack();
            },
        });
    },
    updateSeek (_this) {
        let player = _this.getInstance()
        if (!player.playerInstance) return;
        player.currentSecond = player.formatTime(player.playerInstance.seek()) || 0;
        // this.seekTrack(this.playerInstance.seek())
        if (player.playerInstance.playing()) {
            requestAnimationFrame(player.updateSeek);
        }
    },
    getInstance () {
        return this
    },
    formatTime(seconds) {
        let minutes = Math.floor(seconds / 60)
        let sec = Math.round(seconds - minutes * 60)
        if (sec < 10) sec = '0' + sec
        return minutes + ':' + sec
    }
}
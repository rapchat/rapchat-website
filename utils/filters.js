export default {
  formatTitle (title) {
    if (title) {
      title = title.trim()
    } else {
      return 'Untitled'
    }
    if (title[0] === '#') {
      if (title.length > 40) {
        return title.slice(0,40).concat('...')
      } else {
        return title.slice(0,40)
      }
    } else {
      let newTitle = title.split("#")
      if (newTitle[0].length > 40) {
        return newTitle[0].slice(0,40).concat('...')
      } else {
        return newTitle[0]
      }
    }
  },
  bigNumber (number) {
    if (number >= 1000000000) {
      return (number / 1000000000).toFixed(1).replace(/\.0$/, '') + 'G'
    }
    if (number >= 1000000) {
      return (number / 1000000).toFixed(1).replace(/\.0$/, '') + 'M'
    }
    if (number >= 1000) {
      return (number / 1000).toFixed(1).replace(/\.0$/, '') + 'K'
    }
    return number
  },
  location (loc) {
    return `${loc.city? loc.city+', ' : ''}${loc.state ? loc.state+', ' : ''}${loc.country}`
  },
  trimBio (text) {
    if (text) {
      if (text.length >300) {
        let trimmed = text.slice(0, 299)
        return trimmed.concat('...')
      } else {
        return text
      }
    } else {
      return 'Just out here doing my thing!'
    }
  }
}

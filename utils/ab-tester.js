import PlanOut from 'planout'

export class ButtonTest extends PlanOut.Experiment {
  configureLogger(){
    return;
  }
  log (event) {
    console.log (event)
  }
  previouslyLogged(){
    //check if we alerady logged an event for this user
    //return this._exposureLogged is a sane default for client-side experiments
  }
  setup () {
    this.setName('ButtonTest')
  }
  getParamNames () {
    return ['button-text']
  }
  assign(params, args) {
    params.set('buttonCopy', new PlanOut.Ops.Random.UniformChoice({choices:['Recorded on the','Rap on this beat with the'], unit:args.userId}))
  }
}

export class ColorTest extends PlanOut.Experiment {
  configureLogger(){
    return;
  }
  log(event){
    console.log(event)
  }
  previouslyLogged(){
    //check if we alerady logged an event for this user
    //return this._exposureLogged is a sane default for client-side experiments
  }
  setup(){
    this.setName('ColorTest')
  }
  getParamNames(){
    return ['button-color']
  }
  assign(params, args){
    params.set('buttonColor', new PlanOut.Ops.Random.UniformChoice({choices:['#8c8046','#35A15F'], unit:args.userId}))
  }
}

export class PopoverTest extends PlanOut.Experiment {
  configureLogger () {
    return;
  }
  log (event) {
    console.log(event)
  }
  setup () {
    this.setName('PopoverTest')
  }
  getParamNames () {
    return ['modal-copy']
  }
  assign (params, args) {
    params.set('showPopover', new PlanOut.Ops.Random.UniformChoice({choices: [true, false], 'unit': args.userId}))
  }
}

export class ShareRapPageTest extends PlanOut.Experiment {
  configureLogger () {
    return
  }
  log (event) {
    console.log(event)
    return event
  }
  setup () {
    this.setName('Share Rap Landing Test')
  }
  previouslyLogged () {
    //
  }
  getParamNames () {
    return this.getDefaultParamNames()
  }
  assign (params, args) {
    params.set('variation', new PlanOut.Ops.Random.UniformChoice({
      choices: [true, false], 'unit': args.userId
    }))
  }
}
export class ButtonCopyTest extends PlanOut.Experiment {
  configureLogger () {
    return
  }
  log (event) {
    console.log(event)
    return event
  }
  previouslyLogged () {
    //
  }
  setup () {
    this.setName('Button Copy Test')
  }
  getParamNames () {
    return this.getDefaultParamNames()
  }
  assign (params, args) {
    params.set('copy', new PlanOut.Ops.Random.UniformChoice({
      choices: [
        'JOIN THE GAME',
        'REPLY',
        'RECORD YOUR OWN'
      ],
      'unit': args.userId
    }))
  }
}
